package com.highway.roadSystem;

public class HighwayBean {

	private String typeOfHighway;
	private String typeOfHighwayOnly;
	private String direction;
	private String quadrant;
	private int numberOfHighway;

	public HighwayBean(String typeOfHighwayOnly, int numberOfHighway) {
		super();
		this.typeOfHighwayOnly = typeOfHighwayOnly;
		this.numberOfHighway = numberOfHighway;
	}

	public HighwayBean(String typeOfHighway) {
		super();
		this.typeOfHighway = typeOfHighway;

	}

	public String getTypeOfHighwayOnly() {
		return typeOfHighwayOnly;
	}

	public void setTypeOfHighwayOnly(String typeOfHighwayOnly) {
		this.typeOfHighwayOnly = typeOfHighwayOnly;
	}

	public int getNumberOfHighway() {
		return numberOfHighway;
	}

	public void setNumberOfHighway(int numberOfHighway) {
		this.numberOfHighway = numberOfHighway;
	}

	public String getTypeOfHighway() {
		return typeOfHighway;
	}

	public void setTypeOfHighway(String typeOfHighway) {
		this.typeOfHighway = typeOfHighway;
	}

	public String getDirection() {
		return direction;
	}

	public void setDirection(String direction) {
		this.direction = direction;
	}

	public String getQuadrant() {
		return quadrant;
	}

	public void setQuadrant(String quadrant) {
		this.quadrant = quadrant;
	}

}
