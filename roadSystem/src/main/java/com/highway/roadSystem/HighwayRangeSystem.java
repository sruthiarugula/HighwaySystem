package com.highway.roadSystem;

public class HighwayRangeSystem {

	public HighwayBean rangeChecker(String string) throws IllegalArgumentException {
		// TODO Auto-generated method stu
		HighwayBean bean;
		int num = 0;
		String[] st = string.split("-");
		num=Integer.parseInt(st[1]);

		if (num< 1 || num > 99) {
			throw new IllegalArgumentException("Highway Numberout of range");

		} else {
			bean = new HighwayBean(st[0],num );
		}

		return bean;
	}
}
