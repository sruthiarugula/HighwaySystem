package com.highway.roadSystem;

public class Interstate implements IHighway {

	public String getQuadrant(HighwayBean hbean) {
		// TODO Auto-generated method stub
		String direction = null;
		HighwayRangeSystem highwayRangeSystem = new HighwayRangeSystem();
		HighwayBean bean = highwayRangeSystem.rangeChecker(hbean.getTypeOfHighway());
		String roadType = bean.getTypeOfHighwayOnly();
		int roadNumber = bean.getNumberOfHighway();

		if (roadType.equalsIgnoreCase("I") && (roadNumber % 2 == 0) && (roadNumber >= 2 && roadNumber <= 48)) {
			direction = "South";
		} else if (roadType.equalsIgnoreCase("I") && (roadNumber % 2 == 1) && (roadNumber >= 1 && roadNumber <= 49)) {
			direction = "West";
		} else if (roadType.equalsIgnoreCase("I") && (roadNumber % 2 == 1) && (roadNumber >= 51 && roadNumber <= 99)) {
			direction = "East";
		} else if (roadType.equalsIgnoreCase("I") && (roadNumber % 2 == 0) && (roadNumber >= 50 && roadNumber <= 98)) {
			direction = "North";
		}
		return direction;
	}

}
