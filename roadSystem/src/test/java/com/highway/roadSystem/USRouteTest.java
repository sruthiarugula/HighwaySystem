package com.highway.roadSystem;

import static org.junit.Assert.*;

import org.junit.Test;

public class USRouteTest {

	@Test
	public void USRouteSouthEvenNumberCheck() {

		IHighway ihighway = new USRoute();
		assertEquals("North", ihighway.getQuadrant(new HighwayBean("US-2")));

	}

	@Test
	public void USRouteEastOddNumberCheck() {

		IHighway ihighway = new USRoute();
		assertEquals("East", ihighway.getQuadrant(new HighwayBean("US-1")));

	}

	@Test
	public void USRouteSouthOddNumberCheck() {

		IHighway ihighway = new USRoute();
		assertEquals("South", ihighway.getQuadrant(new HighwayBean("US-58")));

	}

	@Test
	public void USRouteWestOddNumberCheck() {

		IHighway ihighway = new USRoute();
		assertEquals("West", ihighway.getQuadrant(new HighwayBean("US-69")));

	}
}
