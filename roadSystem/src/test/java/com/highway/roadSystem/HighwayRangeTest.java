package com.highway.roadSystem;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class HighwayRangeTest {
	HighwayBean bean;

	@Before

	public void doSetUp() {
		// TODO Auto-generated method stub
		// new HighwayBean();

	}

	@Rule
	public ExpectedException anException = ExpectedException.none();

	@Test
	// test the commuter for discount of frequentRider
	public void RangeTest() {

		anException.expect(IllegalArgumentException.class);
		anException.expectMessage("Highway Numberout of range");

		HighwayRangeSystem hfs = new HighwayRangeSystem();

		hfs.rangeChecker("I-78");

	}

	@Test
	public void InterstateEvenNumberCheck() {

		IHighway ihighway = new Interstate();
		assertEquals("South", ihighway.getQuadrant(new HighwayBean("I-2")));

	}

	@Test
	public void InterstateOddWestNumberCheck() {

		IHighway ihighway = new Interstate();
		assertEquals("West", ihighway.getQuadrant(new HighwayBean("I-1")));

	}
	@Test
	public void InterstateEvenSouthNumberCheck() {

		IHighway ihighway = new Interstate();
		assertEquals("South", ihighway.getQuadrant(new HighwayBean("I-2")));

	}

	@Test
	public void InterstateWestOddNumberCheck() {

		IHighway ihighway = new Interstate();
		assertEquals("North", ihighway.getQuadrant(new HighwayBean("I-58")));

	}
	@Test
	public void InterstateEastOddNumberCheck() {

		IHighway ihighway = new Interstate();
		assertEquals("East", ihighway.getQuadrant(new HighwayBean("I-69")));

	}
}
